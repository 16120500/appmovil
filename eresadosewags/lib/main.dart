import 'package:eresadosewags/src/pages/home_page.dart';
import 'package:eresadosewags/src/routes/routes.dart';
import 'package:flutter/material.dart';


 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Egresados WASG',
      initialRoute: '/',
      routes: getAplicationRouts(),

      onGenerateRoute: ( RouteSettings senttings ) {

          print( 'ruta llamada  ${ senttings.name }');
          return MaterialPageRoute(
            builder: ( BuildContext context ) => HomePage()
          );
      },
    );
  }
}