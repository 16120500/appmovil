import 'package:flutter/material.dart';


final _icons = <String, IconData> {

  'notifications'     : Icons.notifications,
  'assignment'        : Icons.assignment,
  'remove_red_eye'    : Icons.remove_red_eye,
  'assessment'        : Icons.assessment,
  // 'input'         : Icons.input,
  // 'tune'          : Icons.tune,
  // 'list'          : Icons.list,
};

Icon getIcon( String nombreIcon ) {

  return Icon( _icons[ nombreIcon ], color: Colors.blueAccent );

}