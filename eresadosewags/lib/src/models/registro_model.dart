// To parse this JSON data, do
//
//     final registroModel = registroModelFromJson(jsonString);

// import 'dart:convert';

// RegistroModel registroModelFromJson(String str) => RegistroModel.fromJson(json.decode(str));

// String registroModelToJson(RegistroModel data) => json.encode(data.toJson());

class RegistroModel {
    String id;
    String nombre;
    String apellidoP;
    String apellidoM;
    String telefono;
    String numControl;
    String carrera;
    String comprobante;

    RegistroModel({
        this.id,
        this.nombre,
        this.apellidoP,
        this.apellidoM,
        this.telefono,
        this.numControl,
        this.carrera,
        this.comprobante,
    });

    factory RegistroModel.fromJson(Map<String, dynamic> json) => RegistroModel(
        id            : json["id"],
        nombre        : json["nombre"],
        apellidoP     : json["apellidoP"],
        apellidoM     : json["apellidoM"],
        telefono      : json["telefono"],
        numControl    : json["numControl"],
        carrera       : json["carrera"],
        comprobante   : json["comprobante"],
    );

    Map<String, dynamic> toJson() => {
        "id"         : id,
        "nombre"     : nombre,
        "apellidoP"  : apellidoP,
        "apellidoM"  : apellidoM,
        "telefono"   : telefono,
        "numControl" : numControl,
        "carrera"    : carrera,
        "comprobante": comprobante,
    };
}