

import 'package:eresadosewags/src/pages/estadisticas_page.dart';
import 'package:eresadosewags/src/pages/generar_programa_page.dart';
import 'package:eresadosewags/src/pages/home_page.dart';
import 'package:eresadosewags/src/pages/pagos_realizados_pages.dart';
import 'package:eresadosewags/src/pages/ver_programa_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getAplicationRouts() {

  return <String, WidgetBuilder> {
    '/'                   :    ( BuildContext context ) => HomePage(),
    'pagosRealizados'     :    ( BuildContext context ) => PagosRealizadosPage(),
    'generarPrograma'            :    ( BuildContext context ) => GenerarProgramaPage(),
    
    'verPrograma'          :    ( BuildContext context ) => VerProgramaPage(),
    'estadisticas'        :    ( BuildContext context ) => EstadisticasPage(),
    //'avatar'              :    ( BuildContext context ) => AvatarPage(),
  };

}

