//import 'dart:js';

import 'package:eresadosewags/src/providers/menu_provider_page.dart';
import 'package:eresadosewags/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class MenuPages extends StatelessWidget {

  //final opciones = ['uno', 'dos', 'tres', 'cuatro'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Egresados WASG'),
        centerTitle: true,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://as01.epimg.net/mexico/imagenes/2019/11/12/tikitakas/1573575489_510774_1573575583_noticia_normal.jpg'),
              radius: 20.0,
            ),
          ),
        ],
      ),
      
      drawer: Drawer(
        
        
        child: _lista(),
        
      ),
    );
  }

  Widget _lista(){

    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot) {

        return ListView(
          children: _crearItems( snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _crearItems(List<dynamic> data, BuildContext context ) {

    //print(menuProvider.opciones);

    final List<Widget> opciones = [
      DrawerHeader( 
      decoration: BoxDecoration(
        color: Colors.blue,
      ),
      child: Text(
        'Jonathan',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24,
        ),
      ),
    )

    ];
    
    data.forEach( ( opt ) {
      final widgetTemp = ListTile(
        title: Text( opt['texto']),
        leading: getIcon(opt['icon']),
        onTap: () {

           

          Navigator.pushNamed(context, opt['ruta']);
        },
      );
      opciones..add( widgetTemp)
              ..add(Divider()) ;
    });

    return opciones;

    

            


  }
}