import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar'),
        centerTitle: true,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://as01.epimg.net/mexico/imagenes/2019/11/12/tikitakas/1573575489_510774_1573575583_noticia_normal.jpg'),
              radius: 20.0,
            ),
          ),

          Container(
            margin: EdgeInsets.only( right: 10.0 ),
            child: CircleAvatar(
              child: Text('JA'),
              backgroundColor: Colors.purple,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
        image: NetworkImage('https://www.larepublica.net/storage/images/2018/11/12/20181112131437.stan-lee.jpg'),
        placeholder: AssetImage('assets/loader.gif'),
        fadeInDuration: Duration( milliseconds: 200 ),
          ),
      ),
    );
  }
}  