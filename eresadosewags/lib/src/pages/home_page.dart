import 'package:eresadosewags/src/pages/Menu_Pages.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {

  //final _nombreUsuario = 'Jonathan';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( 
        title: Text(''), 
      ),
       drawer: MenuPages(),
    );
  }
}